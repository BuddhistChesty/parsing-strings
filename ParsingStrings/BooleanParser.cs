﻿using System;

namespace ParsingStrings
{
    public static class BooleanParser
    {
        public static bool TryParseBoolean(string? str, out bool result)
        {
            return bool.TryParse(str, out result);
        }

        public static bool ParseBoolean(string? str)
        {
            if (str == null)
            {
                throw new ArgumentNullException(nameof(str));
            }

            try
            {
                return bool.Parse(str);
            }
            catch (FormatException)
            {
                return false;
            }
        }
    }
}
